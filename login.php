<?php
	require_once('core/init.php');
	include_once('includes/header.php');
	include_once('includes/flash-success-error.php');
	if(is_loggedin()){
		redirect_to('dashboard.php');
	}
	//var_dump(is_loggedin());
	if(input_exists() AND token_matches(input_get('token'))){
		$validation_rules = [
			'identity' => [
				'required' => true,
				'min' => 2,
				'max' => 100
			],
			'password' => [
				'required' => true,
				'min' => 4
			]

		];
		
		$validation_errors = validation_check($_POST, $validation_rules );

		if(!count($validation_errors)){
			//validation succeed, now login
			$identity = input_get('identity');
			$password = input_get('password');
			$login = login($identity, $password);
			if($login){
				redirect_to('dashboard.php');
			}
		}
		

	}
?>

<?php include_once('includes/flash-success-error.php'); ?>

<form action="" method="post">
	<!-- validation errors -->
	<?php include_once('includes/validation-errors.php'); ?>

	
	<fieldset>
		<legend>Login</legend>

		<!-- identity -->
		<div>
			<label for="identity">Email/username</label>
			<input type="text" name="identity" id="identity" value="<?= input_get('identity')?>">
		</div>
		<!-- password -->
		<div>
			<label for="password">Password</label>
			<input type="password" name="password" id="password" >
		</div>

		<input type="hidden" name="token" value="<?= token_generate()?>">
		<!-- submit -->
		<div>
			<input type="submit" value="Login">
		</div>

	</fieldset>
</form>


<!-- include footer -->
<?php include_once('includes/footer.php')?>