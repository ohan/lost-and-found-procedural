<?php
	require_once('core/init.php');
	access_to('loggedin');
	include_once('includes/header.php');


	$user_id = session_get(config_get('session/session_name'));
	$profile = get_profile($user_id);


	if(input_exists() AND token_matches(input_get('token'))){
		$validation_rules = [
			'first_name' => [
				'required' => true,
				'min' => 2,
				'max' => 20
			],
			'last_name' => [
				'required' => true,
				'min' => 2,
				'max' => 20
			],
			'telephone' => [
				'required' => true,
				'exact' => 11
			]
	
		];

		$validation_errors = validation_check($_POST, $validation_rules );

		if(!count($validation_errors)){
			//validation succeed, now save the data
			$data = [
				'first_name' => input_get('first_name'),
				'last_name' => input_get('last_name'),
				'gender' => input_get('gender'),
				'telephone' => input_get('telephone'),
				'district' => input_get('district'),
				'city' => input_get('city'),
				'zip_code' => input_get('zip_code'),
				'address' => input_get('address')
			];

			//if profile dont exist insert it else update existing
			if(empty($profile['id'])){
				$created = date('Y:m:d H:i:s');
				$data['user_id'] = $user_id;
				$data['created'] = $created;
				$result = db_insert('profiles', $data);
			}else{
				$result = db_update('profiles', $profile['id'], $data);
				
			}

			if($result){
				session_flash('flash_success', MSG_SUCCESS_UPDATE);
				redirect_to('dashboard.php');
			}else{
				session_flash('flash_error', MSG_ERROR_UPDATE);
			}

		}
	
	}
?>
<form action="" method="post">
	<!-- validation errors -->
	<?php include_once('includes/validation-errors.php'); ?>


	<fieldset>
		<legend>Edit Profile</legend>
		<!-- first name -->
		<div>
			<label for="first-name">First Name</label>
			<input type="text" name="first_name" id="first-name" value="<?= $profile['first_name']?>">
		</div>

		<!-- last name -->
		<div>
			<label for="last-name">Last Name</label>
			<input type="text" name="last_name" id="last-name" value="<?= $profile['last_name']?>">
		</div>

		<!-- gender -->
		<div>
			<label for="gender">Gender</label>
			<label for="gender-male">
				<input type="radio" name="gender" id="gender-male" value="male" <?php if($profile['gender'] == 'male') echo 'checked'?>>Male
			</label>
			<label for="gender-female">
				<input type="radio" name="gender" id="gender-female" value="female" <?php if($profile['gender'] == 'female') echo 'checked'?>>Female
			</label>

		</div>
		<!-- telephone -->
		<div>
			<label for="telephone">Telephone</label>
			<input type="text" name="telephone" id="telephone" value="<?= $profile['telephone']?>">
		</div>

		<!-- address -->
		<div>
			<label>Address</label>
			<textarea name="address" id="address"><?= $profile['address']?></textarea>
		</div>

		<!-- zip code -->
		<div>
			<label for="zip-code">Zip code</label>
			<input type="text" name="zip_code" id="zip-code" value="<?= $profile['zip_code']?>">
		</div>

		<!-- district -->
		<div>
			<label for="district">District</label>
			<select name="district" id="district">
				<?php foreach(get_all_district() as $district): ?>
					<option value="<?= $district ?>" <?php if($profile['district'] == $district) echo 'selected'?> ><?= $district ?></option>
				<?php endforeach; ?>
			</select>
		</div>

		<!-- city -->
		<div>
			<label for="city">City</label>
			<select name="city" id="city">
				<?php foreach(get_all_city() as $city): ?>
					<option value="<?= $city ?>" <?php if($profile['city'] == $city) echo 'selected'?> ><?= $city ?></option>
				<?php endforeach; ?>
			</select>
		</div>


		<input type="hidden" name="token" value="<?= token_generate()?>">
		<!-- submit -->
		<div>
			<input type="submit" value="proceed">
		</div>

	</fieldset>


</form>



<!-- include footer -->
<?php
include_once('includes/footer.php');
?>