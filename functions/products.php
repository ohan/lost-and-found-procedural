<?php

function generate_product_code(){
	//make a unique id

	do{
		$code = hash_unique();
		$code = substr($code, 0, 6);
		$code = strtoupper($code);

		$result = db_read("SELECT * FROM products WHERE product_code = '{$code}' ");
	}while(count($result));

	return $code;
}


function product_delete($id){
	db_delete('products', $id);
}

function product_list($user_id){
	$products = db_read("SELECT * FROM products WHERE user_id = {$user_id}");
	return $products;
}

function product_get($product_id){
	return db_read_one("SELECT * FROM products WHERE id = '{$product_id}'");
}

function get_product_owner($product_code){
	$product_owner = null;
	$product = db_read_one("SELECT * FROM products WHERE product_code = '{$product_code}'");
	if(!empty($product)){
		$product_owner =  get_user($product['user_id'], true);
	}
	
	return $product_owner;
}