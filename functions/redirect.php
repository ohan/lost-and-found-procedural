<?php
function redirect_to($location = null){
	if($location){
		if(is_numeric($location)){
			switch ($location) {
				case 404:
					header('HTTP/1.1 404 Not Found');
					include('includes/errors/404.php');
					exit();
				break;
				case 401:
					header('HTTP/1.1 401 Unauthorized');
					include('includes/errors/401.php');
					exit();
				break;
				case 500:
					header('HTTP/1.1 500 Internal Server Error');
					include('includes/errors/500.php');
					exit();
				break;
			}
		}
		header('Location: '.$location);
		exit();
	}
}