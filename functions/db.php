<?php
$db_connection = mysqli_connect(config_get('mysql/host'),
								config_get('mysql/username'),
								config_get('mysql/password'),
								config_get('mysql/db'));

if(!$db_connection){
	echo 'unable to connect database';
	exit();
}

//var_dump($db_connection);

//return false or result object
function db_query($query){

	global $db_connection;
	//var_dump($db_connection);
	//var_dump($query);
	return mysqli_query($db_connection, $query);
}

//read array of rows
function db_read($query){
	$result = db_query($query);
	$rows = array();
	if($result){	
		while ($row = mysqli_fetch_assoc($result)) {
			$rows[] = $row; 
		}
	}

	return $rows;
	
}

//read only one row
function db_read_one($query){
	$read_all = db_read($query);
	if(count($read_all)){
		return $read_all[0];
	}
	return array();
}

//return insert id for successful insertion, else false
function db_insert($table, $fields = array()){
	global $db_connection;
	if(!empty($fields)){
		$fields_str = '';
		$values_str = '';
		$x = 1;
		foreach ($fields as $field => $value) {
			$fields_str .= $field;
			$values_str .= "'".$value."'"; 
			if($x < count($fields)){
				$fields_str .= ', ';
				$values_str .= ', ';
			}
			$x++;
		}
	}

	$query = "INSERT INTO {$table} ({$fields_str}) values ({$values_str})";
	db_query($query);
	return mysqli_insert_id($db_connection);
	
}


//return true on successful update
function db_update($table, $id = null, $fields = array()){
	if(!is_null($id) OR !count($fields)){
		$set = '';
		$x = 1;
		foreach ($fields as $field => $value) {
			$set .= $field . ' = '. "'{$value}'";
			if($x < count($fields)){
				$set .= ', ';
			}
			$x++;
		}

		$query = "UPDATE {$table} SET {$set} WHERE id = {$id}";		
		return db_query($query);	
	}
	return false;
}

//params: table name, row id
//return true for successful deletion, else false
function db_delete($table, $id){
	$query = "DELETE FROM $table WHERE id = $id";
	return db_query($query);
}