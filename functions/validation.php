<?php
//returns array of valifation errors
function validation_check($data, $items = array()){
	$errors = array();
	foreach ($items as $item => $rules) {
			foreach ($rules as $rule => $rule_value) {
				$input_value = $data[$item];
				if($rule == 'required' && $rule_value == true && empty($input_value)){
					$errors[] = "{$item} value is required";
				}else if(!empty($input_value)){
					switch ($rule) {
						case 'min':
							if(strlen($input_value) < $rule_value){
								$errors[] = "{$item} must be a minimum length of {$rule_value} chars ";
							}
							break;
						case 'max':
							if(strlen($input_value) > $rule_value){
								$errors[] = "{$item} must be a miximum length of {$rule_value} chars ";
							}
							break;
						case 'exact':
							if(strlen($input_value) != $rule_value){
								$errors[] = "{$item} must be exactly {$rule_value} chars ";
							}
							break;
						case 'matches':
							if($input_value != $data[$rule_value]){
								$errors[] = "{$item} must be a matches {$rule_value} field ";
							}
							break;
						case 'unique':
							$query = "SELECT * FROM {$rule_value} WHERE {$item} = '{$input_value}' ";
							//echo($query);
							$check = db_read($query);
							//var_dump($check);
							if(count($check)){
								$errors[] = "{$item} already exists in {$rule_value}";
							}
							break;
					
						default:
							# code...
							break;
					}
				}
			}
	}

	return $errors;

}

