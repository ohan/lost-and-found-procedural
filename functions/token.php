<?php

//generate a token value
function token_generate(){
	$token_name = config_get('session/token_name');
	$token_value = hash_unique();
	return session_put($token_name, $token_value);
}

//check if input token match the session token
function token_matches($token){
	$token_name = config_get('session/token_name');
	$session_token = session_get($token_name);
	if($token == $session_token){
		session_delete($token_name);
		return true;
	}
	return false;
} 