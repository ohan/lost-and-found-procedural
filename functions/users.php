<?php
function login($identity, $password){
	$password_hash = hash_make($password);
	$query = "SELECT * FROM users WHERE (email = '{$identity}' OR username = '{$identity}' ) AND password = '{$password_hash}'";
	$user = db_read_one($query);
	if(empty($user)){
		session_flash('flash_error', 'Username/Password is wrong');
	}elseif($user['status'] == 'BLOCKED'){
		session_flash('flash_error', 'You has been blocked');
	}else{
		//authenticated
		//var_dump($user);
		$session_name = config_get('session/session_name');
		session_put($session_name, $user['id']);
		return true;
	}
	
	return false;
}

function is_loggedin(){
	$session_name = config_get('session/session_name');
	if(session_exists($session_name)){
		return true;
	}
	return false;
}

function logout(){
	$session_name = config_get('session/session_name');
	if(session_exists($session_name)){
		session_delete($session_name);
	}
}

function get_loggedin_user(){
	$user = null;
	$session_name = config_get('session/session_name');
	if(session_exists($session_name)){
		$user_id = session_get($session_name);
		$user = db_read_one("SELECT * FROM users WHERE id ='{$user_id}'");

		//add profile data
		$profile = db_read_one("SELECT * FROM profiles WHERE user_id = '{$user_id}'");
		if(!empty($profile)){
			$user['profile'] = $profile;
		}

	}

	return $user;
}

function is_admin(){
	if(session_exists(config_get('session/session_name'))){
		$user = get_loggedin_user();
		if($user['is_admin'] == 1){
			return true;
		}
	}

	return false;
}

function get_user($user_id, $with_profile = true){
	$user = null;
	if($user_id ){
		$user = db_read_one("SELECT * FROM users WHERE id ='{$user_id}'");
		$profile = db_read_one("SELECT * FROM profiles WHERE user_id = '{$user_id}'");
		
		//add profile data
		if( $with_profile){
			$user['profile'] = $profile;
		}

	}
	return $user;
}

//get all general VERIFIED/BLOCKED users
function get_general_users(){
	$query = "SELECT users.id, username, first_name, status, count(products.user_id) as total_products
				FROM users
				LEFT JOIN profiles on users.id = profiles.user_id 
				LEFT JOIN products on products.user_id = users.id
				WHERE is_admin = 0 AND users.status != 'NOT_VERIFIED'
				GROUP BY products.user_id ";
	return db_read($query);
}

//authorization function role : general, admin, loggedin 
function access_to($role){
	$user = get_loggedin_user();
	$has_access = false;
	switch ($role) {
		case 'general':
			if($user['is_admin'] == 0)
				 $has_access = true;
		break;
		case 'admin':
			if($user['is_admin'] == 1)
				 $has_access = true;
		break;
		case 'loggedin':
			if(is_loggedin())
				 $has_access = true;
		break;
	}

	if($has_access){
		return;
	}else{
		redirect_to(401);
	}
}

function block_unblock($current_status){
	return ($current_status == 'BLOCKED') ? 'VERIFIED' : 'BLOCKED';
}