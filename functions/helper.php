<?php
//photo upload. return saved path
function upload_photo($field_name, $upload_dir){
	//dd($_FILES);
	$photo_tmp = $_FILES[$field_name]['tmp_name'];
	$photo_id = uniqid();
	$photo_type = $_FILES[$field_name]['type'];
	$photo_path = $upload_dir.'/'.$photo_id;
	//dd($photo_path);
	move_uploaded_file($photo_tmp, $photo_path);
	
	return $photo_path;
}

function delete_photo($photo_path){
	return unlink($photo_path);
}