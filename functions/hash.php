<?php
//make hash. sha512 make 128 chars
function hash_make($text){
	return hash('sha512', $text);
}

//return a unique hash text
function hash_unique(){
	return hash_make(uniqid());
}
