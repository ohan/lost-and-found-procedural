<?php

//check if a request contain $_POST or $_GET data
//default if exists post data
function input_exists($method = 'post'){
	switch ($method) {
		case 'post':
			return (!empty($_POST)) ? true : false;
			break;
		case 'get':
			return (!empty($_GET)) ? true : false;
			break;
		default:
		 	return false;
			break;
	}
}

//get an input value
function input_get($item){
	if(isset($_POST[$item])){
		return $_POST[$item];
	}else if(isset($_GET[$item])) {
		return $_GET[$item];
	}else{
		return '';
	}
}