<?php
define('MSG_SUCCESS_REGISTRATION', 'Congrates! Successfully registered');
define('MSG_FAIL_REGISTRATION', 'Sorry! Registration failed');
define('MSG_FAIL_LOGIN', 'You have entered wrong username/email or password');
define('MSG_SUCCESS_UPDATE', 'update successful!');
define('MSG_ERROR_UPDATE', 'update error!');
define('MSG_SUCCESS_PRODUCT_ADD', 'Product successfully added!');
define('MSG_SUCCESS_PRODUCT_UPDATE', 'Product successfully updated!');