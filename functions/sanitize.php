<?php

//escape the html entities
function escape($string){
	return htmlentities($string, ENT_QUOTES, 'UTF-8');
}