<?php
//check if a cookie is exists
function cookie_exists($name){
	return (isset($_COOKIE[$name])) ? true : false;
}

//get a cookie value
function cookie_get($name){
	if(cookie_exists($name)){
		return $_COOKIE[$name];
	}
	return false;
}

//set a cookie
function cookie_put($name, $value, $expiry){
	if(setcookie($name, $value, time() + $expiry, '/')){
		return true;
	}else{
		return false;
	}
}

//delete a cookie
function cookie_delete($name){
	cookie_put($name, '', time() - 1);
}