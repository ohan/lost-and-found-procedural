<?php

//check if a session exists or not
function session_exists($name = null){
	return (isset($_SESSION[$name])) ? true : false;
}

//get a session value
function session_get($name){
	if(session_exists($name)){
		return $_SESSION[$name];
	}
	return false;
}

//set a session value
function session_put($name, $value){
	return $_SESSION[$name] = $value;
}

//delete a session
function session_delete($name){
	if(session_exists($name)){
		unset($_SESSION[$name]);
		return true;
	}
}

//get or set session flash data
function session_flash($name, $string = ''){
	if(session_exists($name)){
		$data = session_get($name);
		session_delete($name);
		return $data;
	}else{
		session_put($name, $string);
		return '';
	}
}