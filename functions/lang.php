<?php
//load gettext library
require_once('lib/gettext/streams.php');
require_once('lib/gettext/gettext.php');
//get available language set
$lang_set = config_get('lang_set');

if(cookie_exists('lang')){
	$lang = cookie_get('lang');
}else{
	$lang = 'en';
}

if(input_exists('get') AND in_array(input_get('lang'), $lang_set)){
	$lang = input_get('lang');
	cookie_put('lang', $lang, 1*365*24*60*60);	
}

//echo 'language: '.$lang.'<br>';

$lang_file = new FileReader("lang/{$lang}/{$lang}.mo");
$lang_fetch = new gettext_reader($lang_file);


//translation function
function _t($text){
	global $lang_fetch;
	return $lang_fetch->translate($text);
}

