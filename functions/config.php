<?php
//get the config value using a path like mysql/username
// return value
function config_get($path = null){
	if($path){
		$config = $GLOBALS['config'];
		$path = explode('/', $path);
		foreach ($path as $bit) {
			if(isset($config[$bit])){
				$config = $config[$bit];
			}
		}

		return $config;
	}

	return false;
}

//
function get_all_district(){
	$districts = ['chittagong', 'dhaka', 'barishal', 'sylhet'];
	return $districts;
}

function get_all_city(){
	$districts = ['chittagong', 'dhaka', 'barishal', 'sylhet','khulna'];
	return $districts;
}