<?php

function get_profile($user_id){
	
	$query = "SELECT * FROM profiles WHERE user_id = {$user_id}";
	$profile = db_read_one($query);
	//dd($profile);
	if(empty($profile)){
		$profile = [
				'first_name' => '',
				'last_name' => '',
				'gender' => 'male',
				'telephone' => '',
				'district' => '',
				'city' => '',
				'zip_code' => '',
				'address' => ''
		];
	}

	return $profile;
}
