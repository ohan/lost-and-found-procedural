<?php
require_once('core/init.php');
access_to('loggedin');
include_once('includes/header.php');

$user_id = session_get(config_get('session/session_name'));
$products = product_list($user_id);
//d($products);
?>

<h1>List of Products</h1>
<table border="1">
	<thead>
		<tr>
			<th>Sl.</th>
			<th>title</th>
			<th>description</th>
			<th>product code</th>
			<th>bar code</th>
			<th>photo</th>
			<th>actions</th>
		</tr>
	</thead>
	<tbody>
	<?php if(count($products)): $sl = 0;?>
		<?php foreach($products as  $product):?>
		<tr>
			<td><?php echo ++$sl; ?></td>
			<td><?php echo $product['title'] ?></td>
			<td><?php echo $product['description'] ?></td>
			<td><?php echo $product['product_code'] ?></td>
			<td><img src="barcode.php?text=<?= $product['product_code'] ?>" alt="barcode" /></td>
			<td><img width="40" height="40" src="<?= $product['product_picture']?>"></td>
			<td>
				<a href="product-print-tag.php?id=<?php echo $product['id'] ?>"> print </a> |
				<a href="product-delete.php?id=<?php echo $product['id'] ?>"> delete</a> |
				<a href="product-view.php?id=<?php echo $product['id'] ?>">view</a> |
				<a href="product-edit.php?id=<?php echo $product['id'] ?>">Edit</a>
			</td>
		</tr>
		<?php endforeach;?>
	<?php else:?>
		<tr><td colspan="6">No products found!</td></tr>
	<?php endif;?>
	</tbody>
</table>



<!-- include footer -->
<?php include_once('includes/footer.php')?>