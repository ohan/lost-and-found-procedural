<?php
	require_once('core/init.php');
	include_once('includes/header.php');
	if(is_loggedin()){
		redirect_to('dashboard.php');
	}
	if(input_exists() AND token_matches(input_get('token'))){
		$validation_rules = [
			'email' => [
				'required' => true,
				'min' => 2,
				'max' => 100,
				'unique' => 'users'
			],
			'username' => [
				'required' => true,
				'min' => 4,
				'max' => 63,
				'unique' => 'users'
			],
			'password' => [
				'required' => true,
				'min' => 4
			],
			'password_again' => [
				'required' => true,
				'matches' => 'password'
			]

		];
		
		$validation_errors = validation_check($_POST, $validation_rules );

		if(!count($validation_errors)){
			//validation succeed, now save the data
			$password_hash = hash_make(input_get('password'));
			$created = date('Y:m:d H:i:s');
			$data = [
				'email' => input_get('email'),
				'username' => input_get('username'),
				'password' => $password_hash,
				'created' => $created
			];

			$result = db_insert('users', $data);
			//var_dump($result);
			if($result){
				session_flash('flash_success', MSG_SUCCESS_REGISTRATION);
				redirect_to('index.php');
			}else{
				session_flash('flash_error', MSG_FAIL_REGISTRATION);
			}

		}
		

	}
?>
<form action="" method="post">
	<!-- validation errors -->
	<?php include_once('includes/validation-errors.php'); ?>
	
	<fieldset>
		<legend>Registration</legend>

		<!-- email -->
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" id="email" value="<?= input_get('email')?>">
		</div>
		<!-- username -->
		<div>
			<label for="username">Username</label>
			<input type="text" name="username" id="username" value="<?= input_get('username')?>">
		</div>
		<!-- password -->
		<div>
			<label for="password">Password</label>
			<input type="password" name="password" id="password" >
		</div>

		<!-- password again-->
		<div>
			<label for="password-again">Re-enter Password</label>
			<input type="password" name="password_again" id="password-again">
		</div>

		<input type="hidden" name="token" value="<?= token_generate()?>">
		<!-- submit -->
		<div>
			<input type="submit" value="proceed">
		</div>

	</fieldset>


</form>


<!-- include footer -->
<?php include_once('includes/footer.php')?>