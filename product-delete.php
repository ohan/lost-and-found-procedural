<?php
require_once('core/init.php');
access_to('loggedin');
$product_id = input_get('id');
$product = product_get($product_id);
product_delete($product_id);
delete_photo($product['product_picture']);

redirect_to('product-list.php');