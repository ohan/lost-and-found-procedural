<?php
	require_once('core/init.php');
	access_to('loggedin');
	include_once('includes/header.php');

	$product_id = input_get('id');
	$product = db_read_one("SELECT * FROM products WHERE id = '{$product_id}'");
	//dd($product);
	

	if(input_exists() AND token_matches(input_get('token'))){
		$validation_rules = [
			'title' => [
				'required' => true,
				'min' => 2,
				'max' => 20
			],
			'description' => [
				'required' => true,
				'min' => 2
			]
	
		];

		$validation_errors = validation_check($_POST, $validation_rules );

		if(count($validation_errors)){
			//show validation errors
			print_validation_errors($validation_errors);
		}else{
			//validation succeed, now save the data
			$photo_path = $product['product_picture'];

			if(!empty($_FILES['product_picture']['tmp_name'])){
				$photo_path = upload_photo('product_picture', config_get('product_picture_dir'));
				if(!empty($product['product_picture'])){
					delete_photo($product['product_picture']);
				}
				
			}

			$data = [
				'title' => input_get('title'),
				'description' => input_get('description'),
				'product_picture' => $photo_path
			];

			$update = db_update('products', $product_id, $data);
			if ($update) {
				session_flash('flash_success', MSG_SUCCESS_PRODUCT_UPDATE);
				redirect_to('product-list.php');
			}else{
				//redirect_to(404);
			}

		}
	
	}
?>
<form action="" method="post" enctype="multipart/form-data">
	<fieldset>
		<legend>Edit Product</legend>
		<!-- product picture -->
		<div>
			<p><img width="40" height="40" src="<?= $product['product_picture']?>"></p>
			<label for="product_picture"> new product picture</label>
			<input type="file" name="product_picture" id="product_picture">
		</div>
		<!-- title -->
		<div>
			<label for="title">Title</label>
			<input type="text" name="title" id="title" value="<?= $product['title']?>">
		</div>

		<!-- description-->
		<div>
			<label for="description">Description</label>
			<textarea name="description" id="description"><?= $product['description']?></textarea>
		</div>


		<input type="hidden" name="token" value="<?= token_generate()?>">
		<!-- submit -->
		<div>
			<input type="submit" value="proceed">
		</div>

	</fieldset>


</form>



<!-- include footer -->
<?php
include_once('includes/footer.php');
?>