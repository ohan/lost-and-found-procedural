<?php
require_once('core/init.php');
include_once('includes/header.php');

if(input_exists('get')){
	
	$validation_rules = [
			'product_code' => [
				'required' => true,
				'exact' => 6
			]

	];
		
	$validation_errors = validation_check($_GET, $validation_rules );
	if(!count($validation_errors)){
		//validation succeed,
		$product_code = input_get('product_code');
		$product_owner = get_product_owner($product_code);	
	}
	
}
?>
<?php include_once('includes/flash-success-error.php'); ?>

<form action="" method="get">
	<!-- validation errors -->
	<?php include_once('includes/validation-errors.php'); ?>


	<fieldset>
		<legend>Search product</legend>
		<!-- product code -->
		<div>
			<label for="product_code">Enter Product code</label>
			<input type="text" name="product_code" id="title" value="<?= input_get('product_code')?>">
		</div>

		<!-- submit -->
		<div>
			<input type="submit" value="Search">
		</div>
	</fieldset>
</form>

<!-- product owner info -->
<div>
	<?php if(isset($product_owner) AND !empty($product_owner)): ?>
		<?php d($product_owner); ?>
	<?php elseif(input_exists('get')): ?>
		<p>No owner found</p>
	<?php endif ?>
</div>
<!-- include footer -->
<?php include_once('includes/footer.php')?>
