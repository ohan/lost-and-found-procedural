<?php
require_once('core/init.php');
access_to('loggedin');
include_once('includes/header.php');

$user_id = session_get(config_get('session/session_name'));
//dd($user_id);


if(input_exists() AND token_matches(input_get('token'))){
	$validation_rules = [
		'title' => [
			'required' => true,
			'min' => 2,
			'max' => 20
		],
		'description' => [
			'required' => true,
			'min' => 2
		]

	];

	$validation_errors = validation_check($_POST, $validation_rules );

	if(!count($validation_errors)){
		//validation succeed, now save the data
		$product_code = generate_product_code();

		$photo_path = upload_photo('product_picture', config_get('product_picture_dir'));

		$created = date('Y:m:d H:i:s');
		$data = [
			'title' => input_get('title'),
			'description' => input_get('description'),
			'product_code' => $product_code,
			'user_id' => $user_id,
			'product_picture' => $photo_path
		];

		$product_id = db_insert('products', $data);
		if ($product_id) {
			session_flash('flash_success', MSG_SUCCESS_PRODUCT_ADD);
			redirect_to("product-print-tag.php?id={$product_id}");
		}else{
			//redirect_to(404);
		}

	}
}
?>
<form action="" method="post" enctype="multipart/form-data">
	<!-- validation errors -->
	<?php include_once('includes/validation-errors.php'); ?>

	
	<fieldset>
		<legend>Edit Profile</legend>
		<!-- title -->
		<div>
			<label for="title">Title</label>
			<input type="text" name="title" id="title" value="<?= input_get('title')?>">
		</div>

		<!-- description-->
		<div>
			<label for="description">Description</label>
			<textarea name="description" id="description"><?= input_get('title')?></textarea>
		</div>
		<!-- product picture -->
		<div>
			<label for="product_picture"> Upload product picture</label>
			<input type="file" name="product_picture" id="product_picture">
		</div>

		<input type="hidden" name="token" value="<?= token_generate()?>">
		<!-- submit -->
		<div>
			<input type="submit" value="proceed">
		</div>

	</fieldset>


</form>



<!-- include footer -->
<?php include_once('includes/footer.php')?>