<!-- header file-->
<h1>Lost and Found</h1>

<!-- main navigation -->
<nav>
		<!--global  menu-->
		<li><a href="index.php">Home</a></li>
	<?php if(is_loggedin()):?>
		<!-- only loggedin user menu -->
		<li><a href="logout.php">Logout</a></li>
		<li><a href="product-list.php">My Products</a></li>
		<li><a href="product-add.php">Add New Product</a></li>
		<li><a href="profile-edit.php">Edit Profile</a></li>
	<?php else: ?>
		<!-- not loggedin user menu -->
		<li><a href="login.php">Login</a></li>
		<li><a href="registration.php">Registration</a></li>
	<?php endif;?>

	<?php if(is_admin()):?>
		<!-- only admin menu -->
		<li><a href="user-list.php">User List</a></li>
	<?php else: ?>
		<!-- only general user menu -->
		
	<?php endif;?>


</nav>
