
<?php if(!empty($validation_errors)):?>
	<div>
		<?php foreach($validation_errors as $error):?>
			<p><?= $error ?></p>
		<?php endforeach;?>
	</div>
<?php endif;?>