<?php if(session_exists('flash_success')) : ?> 
<div class="flash-success">
	<?= session_flash('flash_success')?>
</div>
<?php endif;?>

<?php if(session_exists('flash_error')) : ?> 
<div class="flash-error">
	<?= session_flash('flash_error')?>
</div>
<?php endif;?>