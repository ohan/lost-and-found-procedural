<?php
//start the session 
session_start();
//global config items
$GLOBALS['config'] = array(
	'mysql' => array(
		'host' => '127.0.0.1',
		'username' => 'root',
		'password' => '',
		'db' => 'lost-and-found-procedural'
	),
	'remember' => array(
		'cookie_name' => 'hash',
		'cookie_expiry' => 3*60*60
	),
	'session' => array(
		'session_name' => 'user',
		'token_name' => 'token'
	),
	'lang_set' => array(
		'english' => 'en',
		'bangla' => 'bn',
		'chittagong' => 'ctg',
		'noakhali' => 'nk'
	),
	'lost_and_found_web_link' => 'http://lostnfound.com',
	'product_picture_dir' => 'product_picture'
);


//set default time zone
date_default_timezone_set('Asia/Dacca');

//load all function files
$functions_dir = 'functions';
$scanned_directory = array_diff(scandir($functions_dir), array('..', '.'));
foreach ($scanned_directory as $file_name) {
	require_once($functions_dir.'/'.$file_name);
}
