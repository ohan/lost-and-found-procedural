<?php
require_once('core/init.php');
access_to('admin');
include_once('includes/header.php');


$users = get_general_users();
//dd($users);
?>

<h1>List of Users</h1>
<table border="1">
	<thead>
		<tr>
			<th>Sl.</th>
			<th>user name</th>
			<th>First name</th>
			<th>status</th>
			<th>total_products</th>
			<th>actions</th>
		</tr>
	</thead>
	<tbody>
	<?php if(count($users)): $sl = 0;?>
		<?php foreach($users as  $user):?>
		<tr>
			<td><?php echo ++$sl; ?></td>
			<td><?php echo $user['username'] ?></td>
			<td><?php echo $user['first_name'] ?></td>
			<td><?php echo $user['status'] ?></td>
			<td><?php echo $user['total_products'] ?></td>
			<td>
				<a href="user-change-status.php?id=<?= $user['id'] ?>&change_status=<?= block_unblock($user['status'])?> "> <?= block_unblock($user['status']) ?></a> |
				<a href="user-view.php?id=<?php echo $user['id'] ?>">view</a> |
			</td>
		</tr>
		<?php endforeach;?>
	<?php else:?>
		<tr><td colspan="9">No user found!</td></tr>
	<?php endif;?>
	</tbody>
</table>



<!-- include footer -->
<?php include_once('includes/footer.php')?>