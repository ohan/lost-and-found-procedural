<?php
require_once('core/init.php');
access_to('loggedin');
include_once('includes/header.php');
$user_id = session_get(config_get('session/session_name'));
$user = db_read_one("SELECT * FROM users WHERE id=".$user_id);
//d($user);
?>

<?php include_once('includes/flash-success-error.php'); ?>

<?php
if($user['is_admin'] == 1){
	include_once('includes/user/admin-dashboard.php');
}else{
	include_once('includes/user/general-dashboard.php');
}


include_once('includes/footer.php');