<?php
require_once('core/init.php');
access_to('admin');
include_once('includes/header.php');


	$user_id = input_get('id');
	$change_status = input_get('change_status');
	db_update('users', $user_id, ['status' => $change_status]);
	
	redirect_to('user-list.php');